%% Exercise 2.4
clear all; clc; close all; % clean up

tmp = matlab.desktop.editor.getActive;  % get location of this script
cd(fileparts(tmp.Filename));            % set working directory to same


dt    = 10^-7; 
gamma = 2*pi*42.577*10^6;

%Allocate the memory needed
nTimeSteps  = 30000;
rfPulse2    = zeros(1,nTimeSteps); %variable to hold a RF waveform with 30000 samples
rfPulse     = zeros(1,nTimeSteps); %variable to hold a RF waveform with 30000 samples
gradAmp     = zeros(1,nTimeSteps); %variable to hold a gradient waveform with 30000 samples
time        = zeros(1,nTimeSteps); %variable to hold 30000 time points

nPosSteps  = 200;
posZ       = zeros(1,nPosSteps); %variable to hold 400 positions allong the z direction
mFinalVect = zeros(nPosSteps,2); %variable to hold the final magnetization calculated for each position
mFinalSmall = zeros(1,nPosSteps); %variable to hold the final magnetization calculated for each position for small tip angle sim


%Generates the time line for plotting
for i=1:nTimeSteps %i starts at 1 go's to 15000
    time(i)    = i*10^-7;                       %Time in seconds
end

%Generate a list of sampling points allong the z direction
for i=1:nPosSteps %i starts at 1 go's to 200
    posZ(i)  = (i-100)*10^-4; %Distance from iso center in meters
end  

%Question A:
%Have to increase slice thickness to reduce gradAmp to 0.0045. This is the
%closest to 0.01m slice thickness possible while maintainning 2 side lobes.
Nlobes = 2;
NLR = 2 + 2*Nlobes;
t0 = 0.002/NLR;
dz = 0.01;

for i=1:20000
    rfPulse(i) = sinc((10000-i)/(t0/dt));
    gradAmp(i) = 2*pi / (gamma*dz*t0);
end

rfPulse(1:20000) = rfPulse(1:20000).*hann(20000)';
A = pi / (gamma*dt*sum(rfPulse));

rfPulse = A*rfPulse;

%refocusing gradient
for i=20000:30000
    gradAmp(i) = -2*pi / (gamma*dz*t0);
end

for j=1:nPosSteps

    mt = 0;
    mz = 1;

    %update dB0AtPosJ:
    dB0AtPosJ = gradAmp(1) * posZ(j);
    
    % start large tip angle starting from [0,0,1]'
    [mt,mz] = fastsim(dB0AtPosJ, rfPulse(1), mt, mz);

    for i=2:nTimeSteps %i starts at 2
    %update dB0AtPosJ:
    dB0AtPosJ = gradAmp(i)*posZ(j);

    % start large tip angle starting from [0,0,1]'
    [mt,mz] = fastsim(dB0AtPosJ, rfPulse(i), mt, mz);
    end

    mFinalVect(j,:) = [mt, mz]; %m(1)+1i*m(2);
end

displaysequence(time, rfPulse, gradAmp)
saveas(gcf,'2p4ASequence.png')
displaysliceprofile(mFinalVect(:,1), mFinalVect(:,2), posZ)
saveas(gcf, '2p4ALargetipangleProfile.png')

max(gradAmp)

%Question B:
%Outside of the slice profile it takes a while for the magnetization to
%relax back to M0. Also the slice profile is not very sharp. This could be
%a problem depending on the resolution we require for our application.

%Question C:
%No, because not all spins rotate at the same Larmor frequency.

%Question D:
%The small tip angle approximation does not show any zeroing at the center
%of the slice profile, which is not physically possible with a 180 degree
%pulse.
for j=1:nPosSteps

    %update dB0AtPosJ:
    dB0AtPosJ = gradAmp(1) * posZ(j);
    m = smalltipangle(dB0AtPosJ, rfPulse(1), 0);

    for i=2:nTimeSteps %i starts at 2
    %update dB0AtPosJ:
    dB0AtPosJ = gradAmp(i)*posZ(j);

    % start large tip angle starting from [0,0,1]'
    m = smalltipangle(dB0AtPosJ, rfPulse(i), m);
    end

    mFinalSmall(j) = m; %m(1)+1i*m(2);
end

figure()
subplot(2, 1, 1)
plot(posZ, abs(mFinalSmall), LineWidth=2, Color='b')
title('Slice Profile')
ylabel('|M_x_y|')
xlabel('z (m)')
legend('MR magnitude')

subplot(2, 1, 2)
plot(posZ, angle(mFinalSmall), LineWidth=2, Color='r')
title('Phase')
ylabel('\angleM_x_y (\circ)')
xlabel('z (m)')
legend('MR phase')

saveas(gcf, '2p4SmalltipangleProfile.png')
