%% Exercise 2.4
clear all; clc; close all; % clean up

tmp = matlab.desktop.editor.getActive;  % get location of this script
cd(fileparts(tmp.Filename));            % set working directory to same


dt    = 10^-7; 
gamma = 2*pi*42.577*10^6;

%Allocate the memory needed
nTimeSteps  = 70000;
nIntermSteps = 40000;
rfPulse     = zeros(1,nTimeSteps); %variable to hold a RF waveform with 30000 samples
gradAmp     = zeros(1,nTimeSteps); %variable to hold a gradient waveform with 30000 samples
time        = zeros(1,nTimeSteps); %variable to hold 30000 time points

nPosSteps  = 200;
posZ       = zeros(1,nPosSteps); %variable to hold 400 positions allong the z direction
mFinalVect = zeros(nPosSteps,2); %variable to hold the final magnetization calculated for each position
mIntermVect = mFinalVect;

%Generates the time line for plotting
for i=1:nTimeSteps %i starts at 1 go's to 15000
    time(i)    = i*10^-7;                       %Time in seconds
end

%Generate a list of sampling points allong the z direction
for i=1:nPosSteps %i starts at 1 go's to 200
    posZ(i)  = (i-100)*10^-4; %Distance from iso center in meters
end  

%Nlobes = 2;
NLR = 3; %2 + 2*Nlobes;
t0= 0.002/NLR;

for i=1:20000
    rfPulse(i) = sinc((10000-i)/(t0/dt));
    gradAmp(i) = 2*pi / (gamma*0.01*t0);
end

rfPulse(1:20000) = rfPulse(1:20000).*hann(20000)';
A = pi / (gamma*dt*sum(rfPulse(1:20000)));

rfPulse(1:20000) = A*rfPulse(1:20000);

for i=20000:40000
    gradAmp(i) = 0.045;
end

NLR = 2;
t0 = 0.002/NLR;

for i=40000:60000
    rfPulse(i) = sinc((50000-i)/(t0/dt));
    gradAmp(i) = 2*pi / (gamma*0.005*t0);
end

rfPulse(40000:60000) = rfPulse(40000:60000).*hann(size(rfPulse(40000:60000),1))';
A = (45*pi/180) / (gamma*dt*sum(rfPulse(40000:60000)));

rfPulse(40000:60000) = A*rfPulse(40000:60000);

%refocusing gradient
for i=60000:70000
    gradAmp(i) = -2*pi / (gamma*0.005*t0);
end

for j=1:nPosSteps

    mt = 0;
    mz = 1;

    %update dB0AtPosJ:
    dB0AtPosJ = gradAmp(1) * posZ(j);
    
    % start large tip angle starting from [0,0,1]'
    [mt,mz] = fastsim(dB0AtPosJ, rfPulse(1), mt, mz);

    for i=2:nIntermSteps
        dB0AtPosJ = gradAmp(i)*posZ(j);
        [mt,mz] = fastsim(dB0AtPosJ, rfPulse(i), mt, mz);
    end

    mIntermVect(j,:) = [mt, mz];
    % assume spoiling of all transverse magnetization
    mt = 0;
    
%     %update dB0AtPosJ:
%     dB0AtPosJ = gradAmp(nIntermSteps) * posZ(j);
%     % start large tip angle starting from [0,0,1]'
%     [mt,mz] = fastsim(dB0AtPosJ, rfPulse(nIntermSteps), 0, 1);

    for i=nIntermSteps+1:nTimeSteps
        %update dB0AtPosJ:
        dB0AtPosJ = gradAmp(i)*posZ(j);
    
        % start large tip angle starting from [0,0,1]'
        [mt,mz] = fastsim(dB0AtPosJ, rfPulse(i), mt, mz);
    end

    mFinalVect(j,:) = [mt, mz]; %m(1)+1i*m(2);
end

%Question A:
displaysequence(time,rfPulse,gradAmp)
saveas(gcf,'2p5ASequence.png')
displaysliceprofile(mIntermVect(:,1), mIntermVect(:,2), posZ)
saveas(gcf,'2p5AIntermSliceprofile.png')
displaysliceprofile(mFinalVect(:,1), mFinalVect(:,2), posZ)
hold on
saveas(gcf,'2p5AFinalSliceProfile.png')

%Question B:
%We weren't able to perfectly invert all of the spins in the imaging slice,
%but this shouldn't be too much of a problem because we crushed all
%unwanted spins.

%%
%Question C:
%The slice profile is improved by wider inversion pulses, with the
%rectangular pulse providing the best final slice profile. However, the
%rectangular pulse does not invert as many of the mZ spins and thus the
%signal outside of the slice is larger with the rectangular pulse.

%2cm slice
rfPulse2     = zeros(1,nTimeSteps); %variable to hold a RF waveform with 30000 samples
gradAmp2     = zeros(1,nTimeSteps); %variable to hold a gradient waveform with 30000 samples
time2        = zeros(1,nTimeSteps); %variable to hold 30000 time points

nPosSteps  = 200;
posZ       = zeros(1,nPosSteps); %variable to hold 400 positions allong the z direction
mFinalVect2 = zeros(nPosSteps,2); %variable to hold the final magnetization calculated for each position
mIntermVect2 = mFinalVect;
dz = 0.02;

%Generates the time line for plotting
for i=1:nTimeSteps %i starts at 1 go's to 15000
    time(i)    = i*10^-7;                       %Time in seconds
end

%Generate a list of sampling points allong the z direction
for i=1:nPosSteps %i starts at 1 go's to 200
    posZ(i)  = (i-100)*10^-4; %Distance from iso center in meters
end  

NLR = 3; %2 + 2*Nlobes;
t0= 0.002/NLR;

for i=1:20000
    rfPulse2(i) = sinc((10000-i)/(t0/dt));
    gradAmp2(i) = 2*pi / (gamma*dz*t0);
end

rfPulse2(1:20000) = rfPulse2(1:20000).*hann(20000)';
A = pi / (gamma*dt*sum(rfPulse2(1:20000)));

rfPulse2(1:20000) = A*rfPulse2(1:20000);

for i=20000:40000
    gradAmp2(i) = 0.045;
end

dz = 0.005;
NLR = 2;
t0 = 0.002/NLR;

for i=40000:60000
    rfPulse2(i) = sinc((50000-i)/(t0/dt));
    gradAmp2(i) = 2*pi / (gamma*dz*t0);
end

rfPulse2(40000:60000) = rfPulse2(40000:60000).*hann(size(rfPulse2(40000:60000),1))';
A = (45*pi/180) / (gamma*dt*sum(rfPulse2(40000:60000)));

rfPulse2(40000:60000) = A*rfPulse2(40000:60000);

%refocusing gradient
for i=60000:70000
    gradAmp2(i) = -2*pi / (gamma*dz*t0);
end

for j=1:nPosSteps

    mt = 0;
    mz = 1;

    %update dB0AtPosJ:
    dB0AtPosJ = gradAmp2(1) * posZ(j);
    
    % start large tip angle starting from [0,0,1]'
    [mt,mz] = fastsim(dB0AtPosJ, rfPulse2(1), mt, mz);

    for i=2:nIntermSteps
        dB0AtPosJ = gradAmp2(i)*posZ(j);
        [mt,mz] = fastsim(dB0AtPosJ, rfPulse2(i), mt, mz);
    end

    mIntermVect2(j,:) = [mt, mz];
    % assume spoiling of all transverse magnetization
    mt = 0;
    
%     %update dB0AtPosJ:
%     dB0AtPosJ = gradAmp(nIntermSteps) * posZ(j);
%     % start large tip angle starting from [0,0,1]'
%     [mt,mz] = fastsim(dB0AtPosJ, rfPulse(nIntermSteps), 0, 1);

    for i=nIntermSteps+1:nTimeSteps
        %update dB0AtPosJ:
        dB0AtPosJ = gradAmp2(i)*posZ(j);
    
        % start large tip angle starting from [0,0,1]'
        [mt,mz] = fastsim(dB0AtPosJ, rfPulse2(i), mt, mz);
    end

    mFinalVect2(j,:) = [mt, mz]; %m(1)+1i*m(2);
end


displaysequence(time,rfPulse2,gradAmp2)
saveas(gcf,'2p5CSequence.png')

subplot(3, 1, 1)
plot(posZ, abs(mIntermVect2(:,1)), LineWidth=2, Color='r')
title('Slice Profile')
ylabel('|M_x_y|')
xlabel('z (m)')
hold on

subplot(3, 1, 2)
plot(posZ, angle(mIntermVect2(:,1)), LineWidth=2, Color='r')
title('Phase')
ylabel('\angleM_x_y (\circ)')
xlabel('z (m)')
hold on

subplot(3, 1, 3)
plot(posZ, mIntermVect2(:,2), LineWidth=2, Color='r')
title('Longitudinal Magnitization')
ylabel('M_z')
xlabel('z (m)')
hold on

%% 

nTimeSteps  = 70000;
nIntermSteps = 40000;
rfPulse3     = zeros(1,nTimeSteps); %variable to hold a RF waveform with 30000 samples
gradAmp3     = zeros(1,nTimeSteps); %variable to hold a gradient waveform with 30000 samples
time        = zeros(1,nTimeSteps); %variable to hold 30000 time points

nPosSteps  = 200;
posZ       = zeros(1,nPosSteps); %variable to hold 400 positions allong the z direction
mFinalVect3 = zeros(nPosSteps,2); %variable to hold the final magnetization calculated for each position
mIntermVect3 = mFinalVect3;

%Generates the time line for plotting
for i=1:nTimeSteps %i starts at 1 go's to 15000
    time(i)    = i*10^-7;                       %Time in seconds
end

%Generate a list of sampling points allong the z direction
for i=1:nPosSteps %i starts at 1 go's to 200
    posZ(i)  = (i-100)*10^-4; %Distance from iso center in meters
end   
%nonselective pulse

for i=1:20000
    rfPulse3(i) = pi / (gamma*20000*dt);
end

for i=20000:40000
    gradAmp3(i) = 0.045;
end

NLR = 2;
t0 = 0.002/NLR;

for i=40000:60000
    rfPulse3(i) = sinc((50000-i)/(t0/dt));
    gradAmp3(i) = 2*pi / (gamma*0.005*t0);
end

rfPulse3(40000:60000) = rfPulse3(40000:60000).*hann(size(rfPulse3(40000:60000),1))';
A = (45*pi/180) / (gamma*dt*sum(rfPulse3(40000:60000)));

rfPulse3(40000:60000) = A*rfPulse3(40000:60000);

%refocusing gradient
for i=60000:70000
    gradAmp3(i) = -2*pi / (gamma*0.005*t0);
end

for j=1:nPosSteps

    mt = 0;
    mz = 1;

    %update dB0AtPosJ:
    dB0AtPosJ = gradAmp3(1) * posZ(j);
    
    % start large tip angle starting from [0,0,1]'
    [mt,mz] = fastsim(dB0AtPosJ, rfPulse3(1), mt, mz);

    for i=2:nIntermSteps
        dB0AtPosJ = gradAmp3(i)*posZ(j);
        [mt,mz] = fastsim(dB0AtPosJ, rfPulse3(i), mt, mz);
    end

    mIntermVect3(j,:) = [mt, mz];
    
    %update dB0AtPosJ:
    dB0AtPosJ = gradAmp3(nIntermSteps) * posZ(j);
    % start large tip angle starting from [0,0,1]'
    [mt,mz] = fastsim(dB0AtPosJ, rfPulse3(nIntermSteps), 0, 1);

    for i=nIntermSteps+1:nTimeSteps
    %update dB0AtPosJ:
    dB0AtPosJ = gradAmp3(i)*posZ(j);

    % start large tip angle starting from [0,0,1]'
    [mt,mz] = fastsim(dB0AtPosJ, rfPulse3(i), mt, mz);
    end

    mFinalVect3(j,:) = [mt, mz]; %m(1)+1i*m(2);
end

displaysliceprofile(mFinalVect(:,1),mFinalVect(:,2),posZ)

subplot(3, 1, 1)
plot(posZ, abs(mFinalVect2(:,1)), LineWidth=2, Color='r')
title('Slice Profile')
ylabel('|M_x_y|')
xlabel('z (m)')
hold on

subplot(3, 1, 2)
plot(posZ, angle(mFinalVect2(:,1)), LineWidth=2, Color='r')
title('Phase')
ylabel('\angleM_x_y (\circ)')
xlabel('z (m)')
hold on

subplot(3, 1, 3)
plot(posZ, mFinalVect2(:,2), LineWidth=2, Color='r')
title('Longitudinal Magnitization')
ylabel('M_z')
xlabel('z (m)')
hold on

subplot(3, 1, 1)
plot(posZ, abs(mFinalVect3(:,1)), LineWidth=2, Color='y')
title('Slice Profile')
ylabel('|M_x_y|')
xlabel('z (m)')
legend("Sinc 1cm", "Sinc 2cm", "Rect")
hold off

subplot(3, 1, 2)
plot(posZ, angle(mFinalVect3(:,1)), LineWidth=2, Color='y')
title('Phase')
ylabel('\angleM_x_y (\circ)')
xlabel('z (m)')
hold off

subplot(3, 1, 3)
plot(posZ, mFinalVect3(:,2), LineWidth=2, Color='y')
title('Longitudinal Magnitization')
ylabel('M_z')
xlabel('z (m)')
hold off

saveas(gcf, "2p5C.png")
%% 

%Question D:
slice = posZ >= -0.005 & posZ <= 0.005;

%0.01 inversion pulse
maxSliceSignal = abs(mFinalVect(posZ==0,1))*sum(slice);
sliceSignal = sum(abs(mFinalVect(slice,1)));
fracSliceSignal = sliceSignal/maxSliceSignal; %0.3359
outsideSliceSignal = sum(abs(mFinalVect(~slice,1)));
maxOutsideSliceSignal = abs(mFinalVect(posZ==0,1))*sum(~slice);
fracOutsideSliceSignal = outsideSliceSignal/maxOutsideSliceSignal; %0.0152

%0.02 inversion pulse
maxSliceSignal2 = abs(mFinalVect2(posZ==0,1))*sum(slice);
sliceSignal2 = sum(abs(mFinalVect2(slice,1)));
fracSliceSignal2 = sliceSignal2/maxSliceSignal2; %0.4142
outsideSliceSignal2 = sum(abs(mFinalVect2(~slice,1)));
maxOutsideSliceSignal2 = abs(mFinalVect2(posZ==0,1))*sum(~slice);
fracOutsideSliceSignal2 = outsideSliceSignal2/maxOutsideSliceSignal2; %0.0046

maxSliceSignal3 = abs(mFinalVect3(posZ==0,1))*sum(slice);
sliceSignal3 = sum(abs(mFinalVect3(slice,1)));
fracSliceSignal3 = sliceSignal3/maxSliceSignal3; %0.4489
outsideSliceSignal3 = sum(abs(mFinalVect3(~slice,1)));
maxOutsideSliceSignal3 = abs(mFinalVect3(posZ==0,1))*sum(~slice);
fracOutsideSliceSignal3 = outsideSliceSignal3/maxOutsideSliceSignal3; %0.0175

%Question E:
%yes, because it would dephase more of the spins outside of the desired slice
%profile.

%Question F:
%To obtain an adequate phase dispersion of 4pi, we can utilize the
%equation gamma*(area of the spoiler gradient)*(slice thickness), where the
%area of the spoiler gradient is equal to it's magnitude times it's duration

%for slice thickness of 0.01:
dz = 0.01;
minDelay = 4*pi / (max(gradAmp)*gamma*dz); %1.0439e-04

%for slice thickness of 0.02
dz = 0.02;
minDelay2 = 4*pi / (max(gradAmp2)*gamma*dz); %5.2193e-05

%for nonselective pulse
minDelay3 = 4*pi / (max(gradAmp3)*gamma); %1.0439e-06


