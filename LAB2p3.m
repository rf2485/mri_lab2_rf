%% Exercise 2.3
clear all; clc; close all; % clean up

tmp = matlab.desktop.editor.getActive;  % get location of this script
cd(fileparts(tmp.Filename));            % set working directory to same

%load sequence
load('sinc_excitation.mat');
nTimeSteps = size(time,2);

%Allocate the memory needed
nPosSteps  = 200;
posZ       = zeros(1,nPosSteps); %variable to hold the positions allong the z direction
mFinalVect = zeros(nPosSteps,2); %variable to hold the final magnetization calculated for each position


%Generate a list of sampling points allong the z direction
for i=1:nPosSteps 
    posZ(i)  = (i-100)*10^-4; %Distance from iso center in meters
end  


%Question A:
figure
displaysequence(time, rfPulse, gradAmp)
saveas(gcf,'2p3A.png')

%Question B:
tic
for j=1:nPosSteps

    mt = 0;
    mz = 1;

    %update dB0AtPosJ:
    dB0AtPosJ = gradAmp(1) * posZ(j);
    
    % start large tip angle starting from [0,0,1]'
    [mt,mz] = fastsim(dB0AtPosJ, rfPulse(1), mt, mz);

    for i=2:nTimeSteps %i starts at 2
    %update dB0AtPosJ:
    dB0AtPosJ = gradAmp(i)*posZ(j);

    % start large tip angle starting from [0,0,1]'
    [mt,mz] = fastsim(dB0AtPosJ, rfPulse(i), mt, mz);
    end

    mFinalVect(j,:) = [mt, mz]; %m(1)+1i*m(2);
end
toc

%Question C:
figure
displaysliceprofile(mFinalVect(:,1), mFinalVect(:,2), posZ)

%Question D:
load("Lab2p1_ref.mat")

subplot(3, 1, 1)
plot(posZ, abs(Lab2p1_ref),'r--', LineWidth=2)
legend('Fast Sim', 'Pre-sim')
hold off

subplot(3, 1, 2)
plot(posZ, angle(Lab2p1_ref), 'r--', LineWidth=2)
legend('Fast Sim', 'Pre-sim')
hold off

saveas(gcf,'2p3D.png')

%Question E:
%Elapsed time is 0.431967 seconds.
%Much faster!
