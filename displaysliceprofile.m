function displaysliceprofile(mt, mz, posZ)

figure()
subplot(3, 1, 1)
plot(posZ, abs(mt), LineWidth=2, Color='b')
title('Slice Profile')
ylabel('|M_x_y|')
xlabel('z (m)')
hold on

subplot(3, 1, 2)
plot(posZ, angle(mt), LineWidth=2, Color='b')
title('Phase')
ylabel('\angleM_x_y (\circ)')
xlabel('z (m)')
hold on

subplot(3, 1, 3)
plot(posZ, mz, LineWidth=2, Color='b')
title('Longitudinal Magnitization')
ylabel('M_z')
xlabel('z (m)')
hold on
