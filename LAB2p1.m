%% Exercise 2.1
clear all; clc; close all; % clean up

tmp = matlab.desktop.editor.getActive;  % get location of this script
cd(fileparts(tmp.Filename));            % set working directory to same

%load sequence
load('sinc_excitation.mat');
nTimeSteps = size(time,2);

%Allocate the memory needed
nPosSteps  = 200;
posZ       = zeros(1,nPosSteps); %variable to hold the positions allong the z direction
mFinal     = zeros(nPosSteps,3); %variable to hold the final magnetization calculated for each position

%Generate a list of sampling points allong the z direction
for i=1:nPosSteps
    posZ(i)  = (i-100)*10^-4; %Distance from iso center in meters
end  


%Question B
figure()
displaysequence(time,rfPulse,gradAmp)
saveas(gcf,'2p1B.png')


%Question D:

tic
for j=1:nPosSteps
    m = [0,0,1]';

    %update dB0AtPosJ:
    dB0AtPosJ = gradAmp(1) * posZ(j);
    
    % start large tip angle starting from [0,0,1]'
    m = largetipangle(dB0AtPosJ, rfPulse(1), m);
    
    for i=2:nTimeSteps %i starts at 2
        
        %update dB0AtPosJ:
        dB0AtPosJ = gradAmp(i)*posZ(j);
        
        %update udpate m:
        m = largetipangle(dB0AtPosJ, rfPulse(i), m);
    end

    mFinal(j,:) = m;
end
toc

%Question E:
load("Lab2p1_ref.mat")

figure()
subplot(3, 1, 1)
plot(posZ, abs(mFinal(:,1)+1i*mFinal(:,2)), LineWidth=2, Color='b')
hold on
plot(posZ, abs(Lab2p1_ref),'r--', LineWidth=2)
title('Slice Profile, Exc 2.1D')
ylabel('|M_x_y|')
xlabel('z (m)')
legend('Sim', 'Pre-sim')
hold off

subplot(3, 1, 2)
plot(posZ, angle(mFinal(:,1)+1i*mFinal(:,2)), LineWidth=2, Color='b')
hold on
plot(posZ, angle(Lab2p1_ref), 'r--', LineWidth=2)
title('Phase')
ylabel('\angleM_x_y (\circ)')
xlabel('z (m)')
legend('Sim', 'Pre-sim')
hold off

subplot(3, 1, 3)
plot(posZ, mFinal(:,3), LineWidth=2, Color='b')
title('Longitudinal Magnitization')
ylabel('M_z')
xlabel('z (m)')
legend('Sim')

saveas(gcf,'2p1E.png')

%Question F:
save("lab2p1f1.mat")

for j=1:nPosSteps
    m = [0,0,1]';

    %update dB0AtPosJ:
    dB0AtPosJ = gradAmp(1) * posZ(j);
    
    % start large tip angle starting from [0,0,1]'
    m = largetipangle(dB0AtPosJ, 20*rfPulse(1), m);
    
    for i=2:nTimeSteps %i starts at 2
        
        %update dB0AtPosJ:
        dB0AtPosJ = gradAmp(i)*posZ(j);
        
        %update udpate m:
        m = largetipangle(dB0AtPosJ, 20*rfPulse(i), m);
    end

    mFinal(j,:) = m;
end
save('lab2p1f20.mat')

load('lab2p1f1.mat')
figure()
plot(posZ, normalize(abs(mFinal(:,1)+1i*mFinal(:,2)), 'range'), LineWidth=2, Color='b')
hold on
load("lab2p1f20.mat")
plot(posZ, normalize(abs(mFinal(:,1)+1i*mFinal(:,2)),'range'), 'r--', LineWidth=2)
title('Slice Profile, Exc 2.1F')
ylabel('|M_x_y|')
xlabel('z (m)')
legend('rfPulse', 'rfPulse * 20')
hold off

saveas(gcf,'2p1F.png')

%The slice profiles are slightly different because the impact of the RF
%pulse on the slice profile is nonlinear for large tip angles. The small
%tip angle approximation would not be able to predict this result because
%it assumes linearity.

%Question G:
%Elapsed time is 1.105310 seconds.
